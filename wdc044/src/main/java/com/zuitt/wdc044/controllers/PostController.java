package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    //route for creating a new post
    @RequestMapping(value= "/posts", method = RequestMethod.POST)
    public ResponseEntity<?> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        postService.createPost(stringToken, post);

        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    //controller for getting all posts
    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<?> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    //This route is for editing posts
    @RequestMapping(value = "/posts/{postid}",method = RequestMethod.PUT)
    public ResponseEntity<?> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken,@RequestBody Post post){
        return postService.updatePost(postid,stringToken,post);
    }
//This route is for deleting a specific post

    @RequestMapping(value = "/posts/{postid}",method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePost(@PathVariable long postid,@RequestHeader(value = "Authorization") String stringToken){
        return postService.deletePost(postid, stringToken);
    }

    // This route is for getting posts by the authenticated user
    @RequestMapping(value = "/posts/myposts", method = RequestMethod.GET)
    public ResponseEntity<?> getPostsByAuthenticatedUser(@RequestHeader(value = "Authorization") String stringToken) {
        Iterable<Post> posts = postService.getPostsByUser(stringToken);
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }

    // Add a new endpoint to get posts by a user's username
    @RequestMapping(value = "/myposts", method = RequestMethod.GET)
    public ResponseEntity<?> getPostsByUser(@RequestHeader(value = "Authorization") String stringToken) {
        return new ResponseEntity<>(postService.getPostsByUser(stringToken), HttpStatus.OK);
    }


}
